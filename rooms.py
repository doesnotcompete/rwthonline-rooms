import datetime
import getopt
import re
import csv
import sys

from bs4 import BeautifulSoup
import requests

from DataStructures import RoomReference, CalendarEntry

roomListUrl = "https://online.rwth-aachen.de/RWTHonline/pl/ui/$ctx;design=ca2;header=max;lang=de;profile=STUDENT;rbacId=/wbRes.cbRessourcenList/NC_8127?pResTypNr=1&pPageNr={}&pListOnly=TRUE"
calendarUrl = "https://online.rwth-aachen.de/RWTHonline/pl/ui/$ctx;design=ca2;header=max;lang=de;profile=STUDENT;rbacId=/wbKalender.cbRessourceKalender/NC_1642?pDisplayMode=w&pDatum={}&pOrgNr=&pResNr={}&pShowAsList=T&pZoom=100&pNurStandardGrp="
humanCalendarUrl = "https://online.rwth-aachen.de/RWTHonline/ee/ui/ca2/app/desktop/#/pl/ui/$ctx/wbKalender.wbRessource?$ctx=design=ca2;header=max;lang=de;profile=STUDENT;rbacId=&pDatum={}&pOrgNr=&pResNr={}&pSachbearbeiter=F"

def extractHTMLFromRoomList(page):
    fullUrl = roomListUrl.format(page)
    data = requests.get(fullUrl).text

    xmlsoup = BeautifulSoup(data, 'lxml')

    instructions = xmlsoup.findAll("instruction", {"action": "replaceElement"})

    if(len(instructions) != 1):
        raise ValueError("No applicable action found in XML response.")

    instruction = instructions.pop()
    return instruction

def getRooms(soup):
    roomItems = soup.find_all("tr", {"class": "coTableR"})

    rooms = []
    for roomItem in roomItems:
        cols = roomItem.find_all("td")
        code = cols[0].find_all("a")[1].contents[0] if(len(cols[0].find_all("a")) >= 1) else "-"
        resourceCodeMatch = re.search(re.compile("(pResNr=\d*)"), str(cols[0].find_all("a")))
        if(not resourceCodeMatch.group(1)):
            print("No match for room {}", code)
            continue
        resourceCode = re.sub(re.compile("(pResNr=)"), "", resourceCodeMatch.group(1))
        name = cols[1].contents[0] if(len(cols[1]) >= 1) else "-"
        seats = int(cols[3].contents[0]) if(len(cols[3]) >= 1) else 0
        street = cols[4].a.contents[0] if(len(cols[4].a) >= 1) else "-"
        room = RoomReference(code, name, seats, street, code, resourceCode)
        rooms.append(room)

    return rooms

def extractHTMLFromCalendar(date, room: RoomReference):
    fullUrl = calendarUrl.format(date, room.resourceCode)
    data = requests.get(fullUrl).text
    xmlsoup = BeautifulSoup(data, 'lxml')

    instructions = xmlsoup.findAll("instruction", {"action": "replaceElement"})

    if(len(instructions) != 1):
        raise ValueError("No applicable action found in XML response.")

    instruction = instructions.pop()
    return instruction

def getCalendarEntries(soup):
    calendarItems = soup.find_all("tr", {"class": "coTableR"})

    calendarEntries = []
    for calendarItem in calendarItems:
        cols = calendarItem.find_all("td")
        dateMatch = re.search(re.compile("(\d{2}.\d{2}.\d{4})"), str(cols[0].contents[1]))
        if(not dateMatch.group(1)):
            print("No match for calendar date.")
            continue
        date = dateMatch.group(1)
        startTime = cols[1].contents[0]
        endTime = cols[2].contents[0]
        labelP = cols[4]
        label = labelP.a.contents[0] if len(labelP.find_all("a")) > 0 else labelP.contents[0]
        start = datetime.datetime.strptime(f"{date}T{startTime}", "%d.%m.%YT%H:%M")
        end = datetime.datetime.strptime(f"{date}T{endTime}", "%d.%m.%YT%H:%M")
        entry = CalendarEntry(start, end, label)
        calendarEntries.append(entry)
    return calendarEntries

def roomAvailable(calendarEntries, start, end):
    for entry in calendarEntries:
        if(max(entry.start, start) < min(entry.end, end)):
            return False
    return True


def main(argv):
    seatmin = 0
    seatmax = 1000
    start = None
    end = None
    try:
        opts, args = getopt.getopt(argv,"l:u:s:e:",["min=","max=","start=","end="])
    except getopt.GetoptError:
        print('rooms.py -s <starttime> -e <endtime> -l <minseats> -u <maxseats>')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-s", "--start"):
            start = datetime.datetime.strptime(arg, "%d.%m.%YT%H:%M")
        elif opt in ("-e", "--end"):
            end = datetime.datetime.strptime(arg, "%d.%m.%YT%H:%M")
        elif opt in ("-l", "--min"):
            seatmin = int(arg)
        elif opt in ("-u", "--max"):
            seatmax = int(arg)

    if(start == None or end == None):
        print('rooms.py -s <starttime> -e <endtime> -l <minseats> -u <maxseats>')
        sys.exit(2)

    allRooms = []
    empty = False
    page = 1
    while(not empty):
        print(f"Now querying resource page {page}...")
        roomXML = extractHTMLFromRoomList(page)
        rooms = getRooms(roomXML)
        if(len(rooms) == 0):
            empty = True
            break
        for room in rooms:
            allRooms.append(room)
        page += 1

    print(f"Found {len(allRooms)} rooms in total. Applying filters...")

    filteredRooms = []
    for room in allRooms:
        if(room.seats >= seatmin and room.seats <= seatmax):
            filteredRooms.append(room)

    print(f"{len(filteredRooms)} rooms satisfy conditions: {seatmin} to {seatmax} seats. Pulling calendars...")

    availableRooms=[]
    for room in filteredRooms:
        calendarXML = extractHTMLFromCalendar(start.strftime("%d.%m.%Y"), room)
        entries = getCalendarEntries(calendarXML)
        if(roomAvailable(entries, start, end)):
            availableRooms.append(room)
            

    with open('rooms.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Code", "Label", "Seats", "Street", "Room Number", "Resource Code", "Link"])
        for room in availableRooms:
            link = humanCalendarUrl.format(start.strftime("%d.%m.%Y"), room.resourceCode)
            writer.writerow([room.code, room.label, room.seats, room.street, room.roomNumber, room.resourceCode, link])

if __name__ == "__main__":
    main(sys.argv[1:])
