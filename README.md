# RWTHonline Room Search
This (currently very minimalist) script scrapes publicly available resource information from RWTHonline and compiles a CSV file with available rooms for a given time slot, including a link to the room calendar. Furthermore, seat constraints can be specified to narrow down the search. 

## Usage
Provided Python 3.6+ is installed on your system, simply run `python rooms.py -s <starttime> -e <endtime> -l <minseats> -u <maxseats>`, with a date format like `23.01.2020T18:00` (`%d.%m.%YT%H:%M`).

For instance, to create a list with all rooms with at least 25 and at most 60 seats available on the 23rd of January 2020 between 12:00 and 14:00, run `python rooms.py -s 23.01.2020T12:00 -e 23.01.2020T14:00 -l 25 -u 60`. The output will be written to `rooms.csv` in the working directory, *any existing file with that name will be overwritten*.
