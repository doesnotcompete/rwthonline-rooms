from dataclasses import dataclass
import datetime


@dataclass(frozen=True)
class RoomReference:
    code: str
    label: str
    seats: int
    street: str
    roomNumber: int
    resourceCode: str


@dataclass(frozen=True)
class CalendarEntry:
    start: datetime
    end: datetime
    label: str
